package ru.cd.maiwifi.utils

/**
 * User: Vlad
 * Date: 07.11.2015
 * Time: 10:18
 *
 *
 * Класс для игнорирования качества ssl сертификатов
 */

import android.annotation.SuppressLint
import org.apache.http.conn.ssl.SSLSocketFactory
import java.io.IOException
import java.net.Socket
import java.security.*
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class MySSLSocketFactory @Throws(NoSuchAlgorithmException::class, KeyManagementException::class, KeyStoreException::class, UnrecoverableKeyException::class)
constructor(truststore: KeyStore) : SSLSocketFactory(truststore) {
    private val sslContext = SSLContext.getInstance("TLS")

    init {
        val tm = object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<X509Certificate>? = null
        }

        sslContext.init(null, arrayOf<TrustManager>(tm), null)
    }

    @Throws(IOException::class)
    override fun createSocket(socket: Socket, host: String, port: Int, autoClose: Boolean): Socket {
        return sslContext.socketFactory.createSocket(socket, host, port, autoClose)
    }

    @Throws(IOException::class)
    override fun createSocket(): Socket {
        return sslContext.socketFactory.createSocket()
    }
}