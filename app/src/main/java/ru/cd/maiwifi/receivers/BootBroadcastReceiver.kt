package ru.cd.maiwifi.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

import ru.cd.maiwifi.MainApplication

/**
 * User: Vlad
 * Date: 10.11.2015
 * Time: 12:06
 *
 *
 * Класс ничего не делает, просто магически стартует приложение при запуске системы
 *
 *
 * upd: и чо ето, нужно протестить
 */
class BootBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val mApplication = context.applicationContext as MainApplication
        Log.d(MainApplication.TAG, "О, привет!")
    }

}