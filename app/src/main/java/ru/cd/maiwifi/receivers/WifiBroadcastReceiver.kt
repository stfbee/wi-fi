package ru.cd.maiwifi.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.NetworkInfo
import android.net.NetworkInfo.State.CONNECTED
import android.net.NetworkInfo.State.DISCONNECTED
import android.os.Parcelable
import ru.cd.maiwifi.MainApplication
import ru.cd.maiwifi.MainService

/**
 * User: Vlad
 * Date: 24.10.2015
 * Time: 22:09
 */
class WifiBroadcastReceiver(paramMainService: MainService) : BroadcastReceiver() {
    private val application: MainApplication = paramMainService.application as MainApplication

    override fun onReceive(paramContext: Context, paramIntent: Intent) {
        when ((paramIntent.getParcelableExtra<Parcelable>("networkInfo") as NetworkInfo).state) {
            CONNECTED -> {
                if (application.connectedWifiName.toLowerCase().contains("mainet_public")) {
                    application.sendConfirmation()
                }
            }
            DISCONNECTED -> application.destroyNotification()
            else -> {
            }
        }
    }
}
