package ru.cd.maiwifi

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder

import ru.cd.maiwifi.receivers.WifiBroadcastReceiver

/**
 * User: Vlad
 * Date: 24.10.2015
 * Time: 22:13
 */
class MainService : Service() {

    private lateinit var wifiReceiver: WifiBroadcastReceiver

    override fun onDestroy() {
        unregisterReceiver(wifiReceiver)
        (application as MainApplication).destroyNotification()
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStart(intent: Intent, startId: Int) {
        wifiReceiver = WifiBroadcastReceiver(this)
        val intentFilter = IntentFilter()
        intentFilter.addAction("android.net.wifi.STATE_CHANGE")
        registerReceiver(wifiReceiver, intentFilter)
    }
}
