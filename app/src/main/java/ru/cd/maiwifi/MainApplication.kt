package ru.cd.maiwifi

import android.app.Application
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.RequestParams
import com.loopj.android.http.TextHttpResponseHandler
import io.fabric.sdk.android.Fabric
import org.apache.http.Header
import org.apache.http.conn.ssl.SSLSocketFactory
import ru.cd.maiwifi.utils.MySSLSocketFactory
import java.security.KeyStore

/**
 * User: Vlad
 * Date: 05.11.2015
 * Time: 6:58
 */
@Suppress("DEPRECATION")
class MainApplication : Application() {

    var status = Status.NONE
    private var service: Intent? = null

    val connectedWifiName: String
        get() {
            val wifiMgr = getSystemService(Context.WIFI_SERVICE) as WifiManager
            val wifiInfo = wifiMgr.connectionInfo
            val ssid = wifiInfo.ssid
            Log.d(TAG, "WLAN name: " + ssid.toLowerCase())
            return ssid
        }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics.Builder().core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build(), Crashlytics())
        Log.d(TAG, "ПОЕХАЛИ!")
        service = Intent(this, MainService::class.java)
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("auto_connect", false)) {
            startService()
        }
    }

    fun sendConfirmation() {
        try {

            // Форсим подключение через вайфай, а то умный андроед пытается подключить нас через мобильную сеть.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

                val request = NetworkRequest.Builder()
                request.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)

                connectivityManager.registerNetworkCallback(request.build(), object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        ConnectivityManager.setProcessDefaultNetwork(network)
                    }
                })
            }

            if (status == Status.CONNECTING || status == Status.CONNECTED) return
            status = Status.CONNECTING

            if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("notify", true)) {
                createNotification(getString(R.string.notification_connect), NOTIFY_ID_CONNECTING)
            } else {
                Toast.makeText(this, R.string.toast_connecting, Toast.LENGTH_SHORT).show()
            }


            val https = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("https", false)

            val asyncHttpClient = AsyncHttpClient()
            val params = RequestParams()
            params.put("buttonClicked", "4")
            params.put("err_flag", "0")
            params.put("err_msg", "")
            params.put("info_flag", "0")
            params.put("info_msg", "")
            params.put("redirect_url", "")
            params.put("network_name", "Guest Network")
            params.put("username", "MAI")
            params.put("password", "1930")

            if (https) {
                val trustStore = KeyStore.getInstance(KeyStore.getDefaultType())
                trustStore.load(null, null)
                val sf = MySSLSocketFactory(trustStore)
                sf.hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER
                asyncHttpClient.setSSLSocketFactory(sf)
            }

            //buttonClicked=4&err_flag=0&err_msg=&info_flag=0&info_msg=&redirect_url=&network_name=Guest+Network&username=MAI&password=1930

            // TODO: 10.11.2015 по нажатию на уведомление открыавть приложение, если ошибка то чонить делать

            asyncHttpClient.isLoggingEnabled = false
            asyncHttpClient.post(this, "http" + (if (https) "s" else "") + "://1.1.1.1/login.html", params, object : TextHttpResponseHandler() {
                override fun onFailure(i: Int, headers: Array<Header>, s: String, throwable: Throwable) {
                    Log.e(TAG, "Fail")
                    status = Status.NONE

                    if (PreferenceManager.getDefaultSharedPreferences(this@MainApplication).getBoolean("notify", true)) {
                        createNotification(getString(R.string.notification_fail), NOTIFY_ID_NONE)
                    } else {
                        Toast.makeText(this@MainApplication, R.string.toast_fail, Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onSuccess(i: Int, headers: Array<Header>, s: String) {
                    Log.i(TAG, "Success")
                    status = Status.CONNECTED

                    if (PreferenceManager.getDefaultSharedPreferences(this@MainApplication).getBoolean("notify", true)) {
                        createNotification(getString(R.string.notification_done), NOTIFY_ID_CONNECTED)
                    } else {
                        Toast.makeText(this@MainApplication, R.string.toast_all_done, Toast.LENGTH_SHORT).show()
                    }
                }
            })

        } catch (e: Exception) {
            Log.e(TAG, "oops, i did it again", e)
            Toast.makeText(this, R.string.toast_exception, Toast.LENGTH_SHORT).show()
        }

    }

    fun createNotification(text: String, NOTIFY_ID: Int) {
        destroyNotification()
        val builder = Notification.Builder(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            when (status) {
                Status.CONNECTED -> builder.setColor(resources.getColor(R.color.colorPrimary))
                Status.CONNECTING -> builder.setColor(resources.getColor(R.color.colorConnecting))
                Status.NONE -> builder.setColor(resources.getColor(R.color.colorFail))
            }
        }


        val notificationIntent = Intent(applicationContext, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val intent = PendingIntent.getActivity(applicationContext, 0, notificationIntent, 0)

        val notification = builder.setSmallIcon(R.drawable.icon_notification)
                .setTicker(getString(R.string.app_name) + ": " + text)
                .setContentTitle(getString(R.string.app_name))
                .setWhen(System.currentTimeMillis())
                .setContentText(text)
                .setContentIntent(intent)
                .notification

        notification.flags = notification.flags or Notification.FLAG_AUTO_CANCEL

        (this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(NOTIFY_ID, notification)
    }

    fun destroyNotification() {
        (this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(NOTIFY_ID_CONNECTED)
        (this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(NOTIFY_ID_CONNECTING)
        (this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(NOTIFY_ID_NONE)
    }

    fun startService() {
        startService(service)
    }

    fun stopService() {
        stopService(service)
    }

    enum class Status {
        NONE, CONNECTING, CONNECTED
    }

    companion object {
        const val TAG = "MaiWiFi"
        const val NOTIFY_ID_CONNECTED = 420
        const val NOTIFY_ID_CONNECTING = 228
        const val NOTIFY_ID_NONE = 214
    }
}
