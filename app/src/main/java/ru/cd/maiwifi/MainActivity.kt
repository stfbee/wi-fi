package ru.cd.maiwifi

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val application = application as MainApplication

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        setContentView(R.layout.activity_main)

        auto_connect_switch.isChecked = preferences.getBoolean("auto_connect", false)
        notify_switch.isChecked = preferences.getBoolean("notify", true)
        https_switch.isChecked = preferences.getBoolean("https", false)
        version.text = getString(R.string.version, BuildConfig.VERSION_NAME)

        auto_connect_switch.setOnCheckedChangeListener { _, isChecked ->
            preferences.edit().putBoolean("auto_connect", isChecked).apply()
            if (isChecked) {
                application.startService()
            } else {
                application.stopService()
            }
        }

        notify_switch.setOnCheckedChangeListener { _, isChecked ->
            preferences.edit().putBoolean("notify", isChecked).apply()
            if (!isChecked) application.destroyNotification()
        }

        https_switch.setOnCheckedChangeListener { _, isChecked -> preferences.edit().putBoolean("https", isChecked).apply() }

        auth_button.setOnClickListener {
            if (application.connectedWifiName.toLowerCase().contains("mainet_public")) {
                application.sendConfirmation()
            } else {
                Toast.makeText(this@MainActivity, R.string.toast_nope, Toast.LENGTH_SHORT).show()
            }
        }

        vk_link.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(getString(R.string.vk_link_full))
            startActivity(i)
        }
    }
}
